## Docker Image:
```
https://hub.docker.com/r/taihotech/centos-nginx-nodejs/
```

## Gitignore
```sh
wget https://www.gitignore.io/api/node%2Cmacos%2Cwindows -O ./.gitignore
```

## Build Docker
```sh
cd docker

# Build
docker build -t centos-nginx-nodejs .

# Run
docker run --name centos-nginx-nodejs -d -p 8080:80 -p 4033:443 centos-nginx-nodejs

# Push
docker build -t taihotech/centos-nginx-nodejs .
docker push taihotech/centos-nginx-nodejs

# Pull
docker run --name centos-nginx-nodejs -d -p 8080:80 -p 4033:443 taihotech/centos-nginx-nodejs
```

## node: command not found
```sh
source ~/.bash_profile
# OR
exec $SHELL -l
```

## Use Ngrok
```sh
su app
cd ~
wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip
unzip ngrok-stable-linux-amd64.zip
echo 'export TERM=xterm' >> ~/.bashrc # only once
echo 'export TERM=xterm' >> ~/.bash_profile # only once
export TERM=xterm
./ngrok http -region=ap 80
```
