#!/bin/bash

# in user app
#echo 'export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin' >> ~/.bashrc

git clone https://github.com/nodenv/nodenv.git ~/.nodenv
#echo 'export PATH="$HOME/.nodenv/bin:$PATH"' >> ~/.bashrc
echo 'export PATH="$HOME/.nodenv/bin:$PATH"' >> ~/.bash_profile

echo 'eval "$(nodenv init -)"' >> ~/.bash_profile
#exec $SHELL -l
git clone https://github.com/nodenv/node-build.git ~/.nodenv/plugins/node-build
#~/.nodenv/bin/nodenv install 6.10.1
#~/.nodenv/bin/nodenv global 6.10.1
~/.nodenv/bin/nodenv install 7.8.0
~/.nodenv/bin/nodenv global 7.8.0
#~/.nodenv/shims/npm i -g yarn forever

#~/.nodenv/shims/yarn
#cd ~
#git clone https://bitbucket.org/Taiho-Tech/koa-demo.git
#cd koa-demo
#~/.nodenv/shims/npm install
