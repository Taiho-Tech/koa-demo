#!/bin/bash

echo | cat << EOS > /etc/yum.repos.d/nginx.repo
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/\$releasever/\$basearch/
gpgcheck=0
enabled=1
EOS

yum update -y
yum install -y nginx

mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.back

echo | cat << EOS2 > /etc/nginx/conf.d/node-app.conf
upstream node-app {
    server localhost:3000;
}

server {
    listen       80;
    server_name  localhost;
    proxy_redirect                          off;
    proxy_set_header Host                   \$host;
    proxy_set_header X-Real-IP              \$remote_addr;
    proxy_set_header X-Forwarded-Host       \$host;
    proxy_set_header X-Forwarded-Server     \$host;
    proxy_set_header X-Forwarded-For        \$proxy_add_x_forwarded_for;
    location / {
        proxy_pass http://node-app/;
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
    root   /usr/share/nginx/html;
    }
}
EOS2
