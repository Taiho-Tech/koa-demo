"use strict"

const http = require('http');
const Koa = require('koa');
const Router = require('koa-router');
const router = new Router();

const app = new Koa();

const port = process.argv[2] || 3000

//console.log(port);
//console.log(process.argv.join(','));

app.use(router.routes()).use(router.allowedMethods());

router
  .get('/', (ctx, next) => {
    ctx.body = '<h1>Hello World<h1>';
    ctx.set({
      'Etag': '1234',
      'Last-Modified': (new Date()),
      'My-Auth-Tag': 'ABCABCABC'
    });
  })
  .get('/:id', (ctx, next) => {
    let id = ctx.params.id

    console.log('ctx.url', ctx.url);
    console.log('ctx.origin', ctx.origin);
    console.log('ctx.href', ctx.href);
    console.log('ctx.path', ctx.path);
    console.log('ctx.query', ctx.query);
    console.log('ctx.querystring', ctx.querystring);
    console.log('ctx.host', ctx.host);
    console.log('ctx.hostname', ctx.hostname);

    console.log('ctx.header', ctx.header);
    let redirectUrl = `/sub/${id}`
    if (ctx.querystring) {
      redirectUrl += `?${ctx.querystring}`
    }
    //ctx.redirect(`/sub/${id}`);
    ctx.redirect(redirectUrl)
    ctx.status = 301;
    //router.redirect('/:id', `/sub/${id}`);
  })
  .get('/sub', (ctx, next) => {
    ctx.body = '<h2>Sub Page<h2>';
  })
  .get('/sub/:id', (ctx, next) => {
    let id = ctx.params.id
    if(id === '404') {
      ctx.throw(404, '{"Test Error" : 404}');
    } else if (id === '500') {
      console.log("ERROR");
      throw `Test Error: ${'{"Test Error" : 500}'}` ;
      //ctx.throw(500, '{"Test Error" : 500}'); //only send `Internal Server Error`, no JSON
    } else {
      ctx.body = `<h2>Sub Page ${id}<h2> <h3>Params: ${JSON.stringify(ctx.query)}</h3>`;
    }
    console.log('Params',ctx.params);
    console.log(`ID: ${id}`);
    console.log('IP', ctx.ip);
    console.log('query', ctx.query);

    //http://localhost:3000/sub/40?a=aaa&b=bbb&c=c&c=2c
    // Params { id: '40' }
    // ID: 40
    // IP ::1
    // query { a: 'aaa', b: 'bbb', c: [ 'c', '2c' ] }
  })

http.createServer(app.callback()).listen(port);
console.log('listening on port ' + port);
