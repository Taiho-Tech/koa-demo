"use strict"
// __dirname

const path = require('path');
const logPath = path.join(__dirname, '../logs');

//console.log(logPath);

const loggerConfig = {
  appenders: [
    {
      type: 'dateFile',
      category: ['access', 'console'], // get msg from stdout
      filename: path.join(logPath, 'access.log') ,
      // pattern: '_yyyy-MM-dd hh:00(O).log',
      pattern: '_yyyy-MM-dd hh:mm(O).log',
      alwaysIncludePattern: false,
      compress: true,
      layout: {
        type    : 'pattern',
        pattern : '[%d{ISO8601_WITH_TZ_OFFSET}] [%p] %c - Host:%h PID:%z - Msg: %m'
      }
    },
    {
      type: 'dateFile',
      category: 'error',
      filename: path.join(logPath, 'error.log'),
      //pattern: '_yyyy-MM-dd hh:00(O).log',
      pattern: '_yyyy-MM-dd hh:mm(O).log',
      alwaysIncludePattern: false,
      compress: true,
      layout: {
        type    : 'pattern',
        pattern : '[%d{ISO8601_WITH_TZ_OFFSET}] [%p] %c - Host:%h PID:%z - Msg: %m - Detal: %x{ln}',
        tokens: { ln : '' }
      }
    },
    {
      type: 'console',
      category: ['access', 'console'], // get msg from stdout
      layout: {
        type: 'pattern',
        pattern : '%[[%d{ISO8601_WITH_TZ_OFFSET}] [%p]%] %c - Host:%h PID:%z - Msg: %m - Detal: %x{ln}',
        tokens: { ln : '' }
      }
    }
  ],
  levels: {
    'access': 'ALL',
    'error': 'ERROR'
  },
  replaceConsole: true // get msg from stdout
};


module.exports = fn => {
  if (fn) {
    loggerConfig['appenders'].forEach((x, i) => {
      try {
        x['layout']['tokens']['ln'] = fn;
      } catch (e) {}
    })
  }
  console.log(fn());
  return loggerConfig;
};
