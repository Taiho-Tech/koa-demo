var log4js = require('log4js');
const loggerConfig = require('./logger_config');

var fn = function() {
  try {
    // The caller:
    return (new Error).stack.split("\n")[1]
    // Just the namespace, filename, line:
    .replace(/^\s+at\s+(\S+)\s\((.+?)([^\/]+):(\d+):\d+\)$/, function (){
        return arguments[1] +' '+ arguments[3] +' line '+ arguments[4];
        // return arguments[0] +' '+ arguments[2] +' line '+arguments[3]
    });
  } catch (e) {

  }
}

// log4js.configure(loggerConfig(()=>{return "AAAA"}));
log4js.configure(loggerConfig(fn));
// var logger = log4js.getLogger('things');
// var logger = log4js.getLogger('dateFile');
var accessLogger = log4js.getLogger('access');
var errorLogger = log4js.getLogger('error');

function sendLog() {
  accessLogger.debug("This little thing went to market");
  accessLogger.info("This little thing stayed at home");
  accessLogger.error("This little thing had roast beef");
  accessLogger.fatal("This little thing had none");
  accessLogger.trace("and this little thing went wee, wee, wee, all the way home.");
  console.log('TEST TEST');
  console.error('TEST ERROR!!');
  console.error(new Error);
  // throw 'ERROR !!! THROW'
  errorLogger.debug("errorLogger This little thing went to market");
  errorLogger.info("errorLogger This little thing stayed at home");
  errorLogger.error("errorLogger This little thing had roast beef");
  errorLogger.fatal("errorLogger This little thing had none");
  errorLogger.trace("errorLogger and this little thing went wee, wee, wee, all the way home.");
}




setInterval(function () {
  sendLog();
}, 5000);
