var log4js = require('log4js');
const $depth = 1

log4js.configure({
  appenders: [ {
      type: "file",
      filename: "important-things.log",
      maxLogSize: 10*1024*1024, // = 10Mb
      numBackups: 5, // keep five backup files
      compress: true, // compress the backups
      encoding: 'utf-8',
      mode: parseInt('0640', 8),
      flags: 'w+'
  },
  {
    type: "dateFile",
    //category: "system",
    filename: "access.log",
    pattern: "_yyyy-MM-dd hh:mm(O).log",
    alwaysIncludePattern: false,
    compress: true,
    layout: {
      type    : "pattern",
      pattern : "[%d{ISO8601_WITH_TZ_OFFSET}] [%p] %c - Host:%h PID:%z - Msg: %m"
    }
  },
  {
     type: "stdout",
    //type: "console",
    //category: "error",
    layout: {
      type    : "pattern",
      level: 'ERROR',
      pattern : "%[[%d{ISO8601_WITH_TZ_OFFSET}] [%p] %] %c - %x{ln} - Host:%h PID:%z %m",
      tokens: {
                ln : function() {
                  try {
                    // The caller:
                    return (new Error).stack.split("\n")[$depth]
                    // Just the namespace, filename, line:
                    .replace(/^\s+at\s+(\S+)\s\((.+?)([^\/]+):(\d+):\d+\)$/, function (){
                        return arguments[1] +' '+ arguments[3] +' line '+ arguments[4];
                        // return arguments[0] +' '+ arguments[2] +' line '+arguments[3]
                    });
                  } catch (e) {

                  }
                }
            }
        }
  } ],
  "levels": {
    "[all]": "INFO",
    "system": "ALL",
    "error": "ERROR"
  }
});
// var logger = log4js.getLogger('things');
var logger = log4js.getLogger('dateFile');
function sendLog() {
  logger.debug("This little thing went to market");
  logger.info("This little thing stayed at home");
  logger.error("This little thing had roast beef");
  logger.fatal("This little thing had none");
  logger.trace("and this little thing went wee, wee, wee, all the way home.");
}

setInterval(function () {
  sendLog();
}, 5000);
